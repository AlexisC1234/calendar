const e = require('express');
const { Events } = require('pg');
const { Event, EventForPay } = require('../Event')

test('l évennement commence le jour et fini le jour', () => {
  const startDate = new Date('March 04, 2021 11:24:00');
  const endDate = new Date('March 04, 2021 17:24:00');

  evennement = new Event();
  evennement.startDate = startDate;
  evennement.endDate = endDate;
  const eventpay_array = evennement.convertEventforPay(evennement);
  expect(evennement.startDate).toBe(eventpay_array[0].startDate);
  expect(evennement.endDate).toBe(eventpay_array[0].endDate);

});

test('l évennement commence la nuit et fini la nuit', () => {
  const startDate = new Date('March 04, 2021 00:24:00');
  const endDate = new Date('March 04, 2021 5:24:00');

  evennement = new Event();
  evennement.startDate = startDate;
  evennement.endDate = endDate;
  const  eventpay_array = evennement.convertEventforPay(evennement);
  expect(evennement.startDate).toBe(eventpay_array[0].startDate);
  expect(evennement.endDate).toBe(eventpay_array[0].endDate);

});

test('l évennement commence le jour et fini la nuit', () => {
  const startDate = new Date('March 04, 2021 15:24:00');
  const endDate = new Date('March 05, 2021 5:24:00');

  evennement = new Event();
  evennement.startDate = startDate;
  evennement.endDate = endDate;
  const eventpay_array = evennement.convertEventforPay(evennement);
  expect(eventpay_array.length).toBe(2);
  expect(eventpay_array[0].startDate.getHours()).toBe(15);
  expect(eventpay_array[1].startDate.getHours()).toBe(22);
  expect(eventpay_array[1].endDate.getHours()).toBe(5);

});



test('l évennement commence la nuit et fini le jour', () => {
  const startDate = new Date('March 05, 2021 00:24:00');
  const endDate = new Date('March 05, 2021 12:24:00');

  evennement = new Event();
  evennement.startDate = startDate;
  evennement.endDate = endDate;
  const eventpay_array = evennement.convertEventforPay(evennement);
  expect(eventpay_array.length).toBe(2);
  expect(eventpay_array[0].startDate.getHours()).toBe(0);
  expect(eventpay_array[1].startDate.getHours()).toBe(7);
  expect(eventpay_array[1].endDate.getHours()).toBe(12); 
});
test('l évennement commence le jour et finit le jour le lendemain', () => {
  const startDate = new Date('March 05, 2021 21:24:00');
  const endDate = new Date('March 06, 2021 8:24:00');

  evennement = new Event();
  evennement.startDate = startDate;
  evennement.endDate = endDate;
  const eventpay_array = evennement.convertEventforPay(evennement);
  expect(eventpay_array.length).toBe(3);
  expect(eventpay_array[0].startDate.getHours()).toBe(21);
  expect(eventpay_array[0].endDate.getHours()).toBe(22);
  expect(eventpay_array[0].startDate.getDate()).toBe(5);
  expect(eventpay_array[0].endDate.getDate()).toBe(5);
  expect(eventpay_array[1].startDate.getHours()).toBe(22);
  expect(eventpay_array[1].endDate.getHours()).toBe(7);
  expect(eventpay_array[1].endDate.getDate()).toBe(6);
  expect(eventpay_array[2].endDate.getHours()).toBe(8);
  expect(eventpay_array[2].endDate.getDate()).toBe(6);
});

test('la date est pendant le jour', () => {
  const startDate = new Date('March 04, 2021 15:24:00');
  const endDate = new Date('March 05, 2021 5:24:00');

  evennement = new Event();
  evennement.startDate = startDate;
  evennement.endDate = endDate;
  const result = evennement.isDuringDay(evennement.startDate);
  expect(result).toBe(true);

});


