const cleanDb = async (db) => {
  await db.Account.truncate({ cascade: true });
  await db.Company.truncate({ cascade: true });
}
module.exports = cleanDb