const express = require('express')
const bodyParser = require('body-parser')
const db = require('./models')

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))

app.use(express.static('app/public'))

app.get('/', (req, res) => {
  res.status(200).send('Hello.')
})

module.exports = app

/*
{
  attendees: [{
    displayName: 'Nick Stock',
    email: client@client.com,
    organizer: true,
    response_status: 'accepted'
  },
    {
      displayName: 'Nicholas Stock',
      email: 'not_client@client.com',
      response_status: 'accepted',
      self: true
    }
  ],
    end: { date_time: DateTime.parse('2018-03-05T18:30:00.000+01:00') },
  html_link: 'https://www.google.com/calendar/event?eid=MGptdjJ1ZDljMWo3Y2kyZzFqZ21ybWY2c3Mgbmlja0BnZW1iYW5pLmNvbQ',
    id: '0jmv2ud9c1j7ci2g1jgmrmf6ss',
  start: { date_time: DateTime.parse('2018-03-05T12:30:00.000+01:00') },
  status: 'confirmed',
    summary: summary
}

EventConverter ->
getId
getStart
getEnd
toHash

BookingModel(payloadHash)

getClientUsers -> si les company id est null
getEmployeeUsers -> si les company id est pas null
getCompany ->
getEventById

if () else

EventModel ->

User
  - name
  - comapny_id

Company
  - name
   - id
 */