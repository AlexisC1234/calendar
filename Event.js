 const startNight = 22
 const startDay = 7

class Event {
    startDate;
    endDate;
    constructor(){
        
    }
 isDuringDay(date){

    if(date.getHours() < startNight && date.getHours() > startDay){
        return true
    }else{
        return false
    }
 }
 convertEventforPay(event){
     

     let eventpay_array = [];
        let eventpay = new EventForPay();

        if (this.isDuringDay(event.startDate) && !this.isDuringDay(event.endDate)){
            let dateStartNight = new Date(event.startDate);
            dateStartNight.setHours(startNight);
            let dateStartDay = new Date(event.endDate);
            dateStartDay.setHours(startDay);
            const new_eventpay = new EventForPay(event.startDate,dateStartNight);
            const new_eventpay2 = new EventForPay(dateStartNight, event.endDate);
            eventpay_array.push(new_eventpay, new_eventpay2);
            
            return eventpay_array;

        }else if(!this.isDuringDay(event.startDate) && this.isDuringDay(event.endDate)){
            let dateStartNight = new Date(event.startDate);
            dateStartNight.setHours(startNight);
            dateStartNight.setMinutes(0);
            let dateStartDay = new Date(event.endDate);
            dateStartDay.setHours(startDay);
            dateStartDay.setMinutes(0);
            const new_eventpay = new EventForPay(event.startDate,dateStartDay);
            const new_eventpay2 = new EventForPay(dateStartDay, event.endDate);
            eventpay_array.push(new_eventpay, new_eventpay2);

            return eventpay_array;
        }else if(this.isDuringDay(event.startDate) && this.isDuringDay(event.endDate) && event.startDate.getDate() != event.endDate.getDate()){
            let dateStartNight = new Date(event.startDate);
            dateStartNight.setHours(startNight);
            dateStartNight.setMinutes(0);
            let dateStartDay = new Date(event.endDate);
            dateStartDay.setHours(startDay);
            dateStartDay.setMinutes(0);
            const new_eventpay = new EventForPay(event.startDate,dateStartNight);
            const new_eventpay2 = new EventForPay(dateStartNight, dateStartDay);
            const new_eventpay3 = new EventForPay(dateStartDay, event.endDate);
            eventpay_array.push(new_eventpay, new_eventpay2, new_eventpay3);

            return eventpay_array;
        }
        else {
            eventpay.startDate = event.startDate;
            eventpay.endDate = event.endDate;
            eventpay_array.push(eventpay);
            return eventpay_array;
        }
        
    }
}

class EventForPay{
    startDate;
    endDate;
    constructor(startDate,endDate){
       this.endDate = endDate;
       this.startDate = startDate;
    }
}

module.exports = {
    Event,
    EventForPay,
};