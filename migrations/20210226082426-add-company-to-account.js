module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Accounts', 'company_id', {
      type: Sequelize.INTEGER,
      references: {
        model: 'Companies', // name of Target model
        key: 'id', // key in Target model that we're referencing
      },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
    })
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('Accounts', 'company_id')
  }
}