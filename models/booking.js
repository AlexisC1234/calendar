'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Booking extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    static updateAndCreateBookingFromPayload(payload) {
      return Booking.findByPk(payload.eventId)
        .then((findedBooking) => {
          if (findedBooking) {
            return findedBooking.update(payload)
          }

          return Booking.create(payload)
        })
        .catch((e) => {
          throw e
        })
    }
  }

  Booking.init({
    start: DataTypes.DATE,
    end: DataTypes.DATE,
    eventId: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Booking',
  });
  return Booking;
};